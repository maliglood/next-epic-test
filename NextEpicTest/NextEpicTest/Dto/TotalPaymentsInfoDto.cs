﻿namespace NextEpicTest.Dto
{
    /// <summary>
    /// Информация обо всех платежах.
    /// </summary>
    public class TotalPaymentsInfoDto
    {
        /// <summary>
        /// Общее количество платежей.
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Общая сумма платежей.
        /// </summary>
        public double TotalSum { get; set; }
    }
}
