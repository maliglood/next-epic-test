﻿using System;

namespace NextEpicTest.Dto
{
    /// <summary>
    /// Добавление платежа.
    /// </summary>
    public class PaymentDto
    {
        /// <summary>
        /// Уникальный идентификатор платежа.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// № счета.
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Сумма платежа.
        /// </summary>
        public double Sum { get; set; }
    }
}
