﻿namespace NextEpicTest.Dto
{
    /// <summary>
    /// Добавление платежа.
    /// </summary>
    public class CreatePaymentDto
    {
        /// <summary>
        /// № счета.
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Сумма платежа.
        /// </summary>
        public double Sum { get; set; }
    }
}
