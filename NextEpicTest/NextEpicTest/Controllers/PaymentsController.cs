﻿using Microsoft.AspNetCore.Mvc;
using NextEpicTest.Dto;
using NextEpicTest.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NextEpicTest.Controllers
{
    /// <summary>
    /// Взаимодействие с платежами.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly IPaymentsService _paymentsService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="paymentsService"></param>
        public PaymentsController(IPaymentsService paymentsService)
        {
            _paymentsService = paymentsService;
        }

        /// <summary>
        /// Получение списка всех платежей.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PaymentDto>> GetAllPaymentsAsync()
        {
            return await _paymentsService.GetPaymentsAsync(cancellationToken: HttpContext.RequestAborted);
        }

        /// <summary>
        /// Получение списка всех платежей по указанному счёту.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{account}")]
        public async Task<List<PaymentDto>> GetAllPaymentsAsync(string account)
        {
            return await _paymentsService.GetPaymentsAsync(account, HttpContext.RequestAborted);
        }

        /// <summary>
        /// Добавление платежей.
        /// </summary>
        /// <param name="payments"> Список платежей. </param>
        /// <returns></returns>
        [HttpPost]
        public async Task<TotalPaymentsInfoDto> AddPaymentAsync(IEnumerable<CreatePaymentDto> payments)
        {
            return await _paymentsService.AddPaymentsAsync(payments, HttpContext.RequestAborted);
        }
    }
}
