import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CreatePayment } from "../models/create-payment.model";
import { Payment } from "../models/payment.interface";
import { Total } from "../models/total.interface";

const configUrl = '/api/payments';

@Injectable()
export class PaymentsHttpService {
    constructor(private http: HttpClient) { }

    public getAllPayments() {
        return this.http.get<Payment[]>(configUrl);
    }

    public getAccountPayments(account: string) {
        return this.http.get<Payment[]>(`${configUrl}/${account}`);
    }

    public addPayments(payments: CreatePayment[]) {
        return this.http.post<Total>(configUrl, payments);
    }
}