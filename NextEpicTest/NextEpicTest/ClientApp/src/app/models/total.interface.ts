export interface Total {
    totalCount: number;
    totalSum: number;
}