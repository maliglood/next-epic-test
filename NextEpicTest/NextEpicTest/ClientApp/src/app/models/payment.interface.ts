export interface Payment {
    id: string;
    account: string;
    sum: number;
}