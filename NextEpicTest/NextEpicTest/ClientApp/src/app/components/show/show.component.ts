import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Payment } from 'src/app/models/payment.interface';
import { PaymentsHttpService } from 'src/app/services/payments-http.service';
import { take } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-show-component',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent {
  
  form: FormGroup;
  
  payments: Payment[] = [];

  constructor (private paymentsHttpService: PaymentsHttpService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({ account: '' });
  }

  showPayments() {
    var account = this.form.controls['account'].value as string;
    this.paymentsHttpService.getAccountPayments(account)
      .pipe(take(1))
      .subscribe(payments => this.payments = payments);
  }
}

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
