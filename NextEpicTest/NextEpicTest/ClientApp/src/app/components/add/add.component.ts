import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';
import { CreatePayment } from 'src/app/models/create-payment.model';
import { Total } from 'src/app/models/total.interface';
import { PaymentsHttpService } from 'src/app/services/payments-http.service';

@Component({
  selector: 'app-add-component',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  form: FormGroup;

  total: Total;

  get payments(): FormArray {
    return this.form.get('payments') as FormArray;
  }

  constructor (private paymentsHttpService: PaymentsHttpService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      payments: this.formBuilder.array([
        this.formBuilder.group({ account: '', sum: 0 })
      ])
    });
  }

  public addPayment() {
    this.payments.push(
      this.formBuilder.group({
        account: null,
        sum: 0
      })
    );
  }

  public save() {
    var payments = this.payments.getRawValue() as CreatePayment[];
    this.paymentsHttpService.addPayments(payments)
      .pipe(take(1))
      .subscribe(total => this.total = total);
  }
}
