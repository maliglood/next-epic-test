import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { Payment } from 'src/app/models/payment.interface';
import { PaymentsHttpService } from 'src/app/services/payments-http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  payments: Payment[] = [];

  constructor (private paymentsHttpService: PaymentsHttpService) { }

  ngOnInit(): void {
    this.paymentsHttpService.getAllPayments()
      .pipe(take(1))
      .subscribe(payments => this.payments = payments);
  }
}
