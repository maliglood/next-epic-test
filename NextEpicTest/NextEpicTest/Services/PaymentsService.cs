﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NextEpicTest.Db;
using NextEpicTest.Db.Models;
using NextEpicTest.Dto;
using NextEpicTest.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NextEpicTest.Services
{
    public class PaymentsService : IPaymentsService
    {
        private readonly NextEpicTestDbContext _context;

        private readonly IMapper _mapper;

        public PaymentsService(IMapper mapper, NextEpicTestDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<TotalPaymentsInfoDto> AddPaymentsAsync(IEnumerable<CreatePaymentDto> paymentsDto, CancellationToken cancellationToken = default)
        {
            var payments = _mapper.Map<List<Payment>>(paymentsDto);
            await _context.Payments.AddRangeAsync(payments, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            var total = await GetTotalInfoAsync(cancellationToken);

            return total;
        }

        public async Task<List<PaymentDto>> GetPaymentsAsync(string account = null, CancellationToken cancellationToken = default)
        {
            var paymentsQuery = _context.Payments.AsQueryable();

            if (!string.IsNullOrEmpty(account))
            {
                var lowerAccount = account.ToLowerInvariant();
                paymentsQuery = paymentsQuery.Where(payment => EF.Functions.Like(payment.Account, $"%{lowerAccount}%"));
            }

            var payments = await paymentsQuery.ToListAsync(cancellationToken);
            var paymentDtos = _mapper.Map<List<PaymentDto>>(payments);

            return paymentDtos;
        }

        private async Task<TotalPaymentsInfoDto> GetTotalInfoAsync(CancellationToken cancellationToken = default)
        {
            var totalInfo = await _context.TotalInfo.FirstAsync(cancellationToken);

            var totalDto = _mapper.Map<TotalPaymentsInfoDto>(totalInfo);

            return totalDto;
        }
    }
}
