﻿using NextEpicTest.Dto;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NextEpicTest.Services.Interfaces
{
    public interface IPaymentsService
    {
        Task<TotalPaymentsInfoDto> AddPaymentsAsync(IEnumerable<CreatePaymentDto> paymentsDto, CancellationToken cancellationToken = default);

        Task<List<PaymentDto>> GetPaymentsAsync(string account = null, CancellationToken cancellationToken = default);
    }
}
