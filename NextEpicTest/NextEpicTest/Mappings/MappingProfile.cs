﻿using AutoMapper;
using NextEpicTest.Db.Models;
using NextEpicTest.Dto;

namespace NextEpicTest.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CreatePaymentDto, Payment>();
            CreateMap<Payment, PaymentDto>();

            CreateMap<TotalInfo, TotalPaymentsInfoDto>();
        }
    }
}
