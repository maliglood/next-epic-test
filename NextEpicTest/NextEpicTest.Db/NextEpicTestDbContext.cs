﻿using Microsoft.EntityFrameworkCore;
using NextEpicTest.Db.Models;

namespace NextEpicTest.Db
{
    public class NextEpicTestDbContext : DbContext
    {
        public DbSet<Payment> Payments { get; set; }

        public DbQuery<TotalInfo> TotalInfo { get; set; }

        public NextEpicTestDbContext(DbContextOptions<NextEpicTestDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var sql = $"select COUNT(*) as TotalCount, SUM({nameof(Payment.Sum)}) as TotalSum from {nameof(Payments)}";
            modelBuilder.Entity<TotalInfo>().ToQuery(() => TotalInfo.FromSqlRaw(sql)); // .HasNoKey()
        }
    }
}
