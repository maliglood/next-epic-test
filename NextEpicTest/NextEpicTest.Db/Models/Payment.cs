﻿using System;

namespace NextEpicTest.Db.Models
{
    public class Payment
    {
        public Guid Id { get; set; }

        public string Account { get; set; }

        public double Sum { get; set; }
    }
}
