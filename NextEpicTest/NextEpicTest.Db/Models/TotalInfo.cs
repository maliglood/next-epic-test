﻿namespace NextEpicTest.Db.Models
{
    public class TotalInfo
    {
        public int TotalCount { get; set; }

        public double TotalSum { get; set; }
    }
}
